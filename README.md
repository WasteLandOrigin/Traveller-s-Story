# Traveller's Story
```v1.10.11-Alpha```
```1.20.1```

## About
Adventure minecraft modpack. This modpack add new dimensions, biomes and mobs. Aliens and space, dungeons and much more!

This modpack will give you a lot of emotions. Starting from realistic village mechanics: conversations, creating a family, professions, guards, ending with a space race and aliens! You also have to protect the earth from arachnids. But be careful with every mob: there is no hardcore bleeding mechanic in the modpack!

## FOSS friendly
#### We try to use mainly FOSS mods. And this project has our custom libre license - [WOPL-v1.0](https://codeberg.org/WasteLandOrigin/WasteLandOrigin-public-license)

![WOPL-license-logo](https://codeberg.org/WasteLandOrigin/WasteLandOrigin-public-license/raw/branch/license/wopl-logo-small.png)

## Work better on LINUX
This modpack will be work better if you using Linux. But MacOS/Windows still compatible.
I work on:
![Debian](https://img.shields.io/badge/Debian-D70A53?style=for-the-badge&logo=debian&logoColor=white) ![Linux Mint](https://img.shields.io/badge/Linux%20Mint-87CF3E?style=for-the-badge&logo=Linux%20Mint&logoColor=white)

## Open wiki
You can edit our modpack wiki!
[Do it!](https://codeberg.org/WasteLandOrigin/Traveller-s-Story/wiki)

## Warning!
#### Turn off the "Use custom end biome source" as shown in the screenshot
![1t](https://github.com/WasteLand-Dev/Traveller-s-Story/assets/76490476/6da5e6fc-8559-4f03-a8d8-a0d4d0f5e82e) ![2t](https://github.com/WasteLand-Dev/Traveller-s-Story/assets/76490476/70fe085a-a2b8-4451-9b8e-e513b035a62e) ![3t](https://github.com/WasteLand-Dev/Traveller-s-Story/assets/76490476/47a9d860-80c6-43d6-8ccf-68294117a95c)

### Can we go through the entire modpack?
The modpack has no logical conclusion, but if you want to test yourself, there are several objectives:
1. Achieve 70 per cent of the achievements related to the dungeons in the Overworld and Nether.
2. Achieve all the achievements related with MCA
3. Get the highest rank in the village
4. Build a tower in a village with different zones
4. Build a space base on every planet
6. Get at least three legendary weapons.
7. Search and explore dungeon with aliens
8. Explore sculk deeps (dimension)

## Community
We are an open community, always ready to help. We have our own discord server with a forum, a github page where you can leave noticed bugs or your suggestions.

## Steam deck and controllers compatibility
- Using [Controlify](https://modrinth.com/mod/controlify)
- Controlify comes with a huge range of settings to fine-tune each of your controllers individually, including vibration strength, every single controller input bind (no hardcoding!) and more.

## Performance
We recommend allocate minimum 8GB of ram for singleplayer game and 6GB for playing on server. And quad core cpu is strongly recommended.

## Community servers
Do you want to add your server to the official list of servers? Send us a message on the discord for this! But there are some rules: 1. No donation, which can affect the gameplay. 2. There is no territory private system. 3. Mods and configs on the server are identical to mods and configs on the client.

## Main modpack features
*In development*

### Changelog:
- New! Add laserbridges
- Disable True Darkness in the End and the Nether
- Remove end-remastered
- Remove immersive-winds
- Remove improved-fire-overlay (maybe temorary)
- Update SimpleMango
- Update AdditionalStructures
- Update AdvancementPlaques
- Update AltOriginGui
- Update AmbientSounds
- Update CleanF3
- Update Companion
- Update CreativeCore
- Update Cull Less Leaves
- Update FarmersDelight
- Update Iceberg
- Update ImmediatelyFast
- Update ItemsDisplayed
- Update Kiwi
- Update MRU
- Update MedievalOriginsRevival
- Update Ping-Wheel
- Update PuzzlesLib
- Update SmartBrainLib
- Update SnowRealMagic
- Update SnowUnderTrees
- Update Species
- Update YetAnotherConfigLib
- Update YungsApi
- Update CTOV
- Update alternate-current
- Update azurelib
- Update balm
- Update betterarcheology
- Update caligo
- Update cc-tweaked
- Update Chipped
- Update cicada
- Update cloth-config
- Update collective
- Update createoreexcavation
- Update crystalchams
- Update darkness
- Update deeperdarker
- Update dummmmmmy
- Update emi
- Update emi_enchanting
- Update emi_loot
- Update entity_texture_features
- Update entityculling
- Update expandeddelight
- Update explorations
- Update exposure
- Update fabric-language-kotlin
- Update fish_of_thieves
- Update frostiful
- Update fzzy_config
- Update garnished
- Update geckolib
- Update graves
- Update hardcorerevival
- Update lithostitched
- Update minecells
- Update minecraft-comes-alive
- Update modernfix
- Update moonlight
- Update naturespirit
- Update noisium
- Update paperdoll
- Update reimaginedmenus
- Update resourcefullib
- Update rpgdifficulty
- Update scorchful
- Update simplyswords
- Update skeletalremains
- Update skinlayers3d
- Update sliceanddice
- Update sodium-shadowy-path-blocks
- Update species
- Update spell_engine
- Update spell_power
- Update spelunkery
- Update thermoo
- Update waystones

## Mod loader
```This modpack works only on fabric!```
[Go to the official fabric website](https://fabricmc.net)

## Social
<p align="center">
    <a href="https://discord.gg/UBaauaN">
        <img src="https://img.shields.io/badge/Discord-%235865F2.svg?&logo=discord&logoColor=white">
            </a>
    <a href="https://mastodon.social/@wlorigin">
        <img src="https://img.shields.io/mastodon/follow/112151761663236004">
            </a>
    <a href="https://mastodon.social/@SpiritOTHawk">
        <img src="https://img.shields.io/mastodon/follow/110688157603004224">
            </a>
    <a href="https://modrinth.com/modpack/travellersstory">
        <img src="https://img.shields.io/modrinth/followers/bl70bvNr">
            </a>
</p>

## Other
```
Tip: install our icon to install in the launcher (launcher_logo.png)
Tip: use these launch options, they will help you to be more efficient :)
Tip: use the latest version of fabric
```

#### Launch options
```
-Xmx8192M -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=50 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=8M -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=10 -XX:G1MixedGCLiveThresholdPercent=50 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=90 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -XX:+UseDynamicNumberOfGCThreads -XX:+AlwaysPreTouch -XX:+ParallelRefProcEnabled -XX:+UseInterpreter -XX:+RewriteFrequentPairs
```

## Other mods used in modpack, for which licences are required
- [Deeper and Darker](https://modrinth.com/mod/deeperdarker)
- [Ordinary Coins](https://www.curseforge.com/minecraft/mc-mods/ordinary-coins) (removed in new versions)
- [Vanilla Degus](https://www.curseforge.com/minecraft/mc-mods/vanilla-degus/files)
- Orb of origin plus: ![Screenshot_20230708-073610_Iceraven](https://github.com/WasteLand-Dev/AdventureTime/assets/76490476/2e36680c-da69-4dde-b787-4f3715dceb74)
- Simply Swords: ![Снимок экрана_2023-07-17_19-43-18](https://github.com/WasteLand-Dev/AdventureTime/assets/76490476/a9a7f0ca-7563-45a9-b124-7c7d51cd43d7)
